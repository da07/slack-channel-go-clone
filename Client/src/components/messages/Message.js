import React, { Component } from "react";
import PropTypes from "prop-types";
//import fecha from "fecha";
import dateFormat from "dateformat";
class Message extends Component {
  render() {
    let { message } = this.props;
    let createdAt = dateFormat(message.createdAt, 'HH:mm:ss YY/MM/DD');

    return (
      <li className="message">
        <div className="author">
          <strong>{message.author}</strong>
          <i className="timestamp">{createdAt}</i>
        </div>
        <div className="body">{message.body}</div>
      </li>
    );

    return <div />;
  }
}

Message.propTypes = {
  message: PropTypes.object.isRequired
};

export default Message;
